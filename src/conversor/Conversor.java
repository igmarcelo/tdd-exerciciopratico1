package conversor;

import java.util.ArrayList;
import java.util.List;

public class Conversor {

	
	public static List<String> converterCamelCase(String original) {
		List<String> lista = new ArrayList<String>();
		String palavras = original.replaceAll("[A-z][a-z]+|([A-Z](?=[a-z][A-Z]))", " $0 ");
		for(String palavra : palavras.split(" ")){
			if(palavraValida(palavra))
				lista.add(formatarPalavra(palavra));
		}
		
		return lista;
	}
	public static boolean palavraValida(String palavra){
		if(!palavra.isEmpty() && palavra.matches("\\w+"))
			return true;
		else
			return false;
	}
	
	public static String formatarPalavra(String palavra){
		if(palavra.matches("[A-Z]{2,}"))
			return palavra.trim();
		else
			return palavra.toLowerCase().trim();
	}
}
