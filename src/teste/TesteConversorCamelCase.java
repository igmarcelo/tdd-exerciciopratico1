package teste;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import conversor.Conversor;

public class TesteConversorCamelCase {

	@Test
	public void convertePalavraCamelCaseParaLista() {
		assertEquals(Arrays.asList("nome"), Conversor.converterCamelCase("nome"));
		assertEquals(Arrays.asList("nome"), Conversor.converterCamelCase("Nome"));
	}
	
	@Test
	public void converteMaisDeUmaPalavraCamelCaseParaLista(){
		assertEquals(Arrays.asList("nome", "composto"), Conversor.converterCamelCase("nomeComposto"));
		assertEquals(Arrays.asList("nome", "composto"), Conversor.converterCamelCase("NomeComposto"));
	}
	
	@Test
	public void coverteSiglaParaLista(){
		assertEquals(Arrays.asList("CPF"), Conversor.converterCamelCase("CPF"));
	}
	
	@Test
	public void covertePalavrasESiglaParaLista(){
		assertEquals(Arrays.asList("numero","CPF"), Conversor.converterCamelCase("numeroCPF"));
		assertEquals(Arrays.asList("numero","CPF"), Conversor.converterCamelCase("NumeroCPF"));
		assertEquals(Arrays.asList("numero","CPF", "contribuinte"), Conversor.converterCamelCase("numeroCPFContribuinte"));
	}
	
	@Test
	public void convertePalavrasNumerosESiglasParaLista(){
		assertEquals(Arrays.asList("recupera","10", "primeiros"), Conversor.converterCamelCase("recupera10Primeiros"));
		assertEquals(Arrays.asList("recupera","10", "primeiros", "CPF"), Conversor.converterCamelCase("recupera10PrimeirosCPF"));
	}

}
